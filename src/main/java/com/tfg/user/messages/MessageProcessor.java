/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages;

import com.tfg.user.Constants;
import com.tfg.user.messages.actions.MessageAction;
import com.tfg.user.messages.actions.MessageDeliveryEnded;
import com.tfg.user.messages.actions.MessagePhase1Part;
import com.tfg.user.messages.actions.MessagePhase2Part;
import com.tfg.user.messages.actions.MessagePlacementEnded;
import com.tfg.user.messages.actions.MessagePlacementPart;
import com.tfg.user.messages.actions.MessageSetup;
import com.tfg.user.utils.SocketReader;
import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class MessageProcessor {

    private HashMap<Integer, MessageAction> ACTIONS = new HashMap<>();

    public MessageProcessor(SocketReader reader) {
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.SETUP, new MessageSetup());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART, new MessagePlacementPart(reader));
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED, new MessagePlacementEnded());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE, new MessagePhase1Part(reader));
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PHASE_2_MESSAGE, new MessagePhase2Part(reader));
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.DELIVEY_ENDED, new MessageDeliveryEnded());
    }

    public void readMessageForType(int type) {
        ACTIONS.get(type).action();
    }
}
