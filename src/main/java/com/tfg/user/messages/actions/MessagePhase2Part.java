/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.user.Constants;
import com.tfg.user.messages.actions.beans.phase1.WFilePhase1;
import com.tfg.user.messages.actions.beans.phase2.MessageToSend;
import com.tfg.user.messages.actions.beans.phase2.WFilePhase2Subpart;
import com.tfg.user.messages.actions.beans.phase2.WFile;
import com.tfg.user.messages.actions.beans.phase2.WFilePhase2;
import com.tfg.user.utils.SocketReader;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePhase2Part implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE;
    private final SocketReader reader;

    public MessagePhase2Part(SocketReader reader) {
        this.reader = reader;
    }

    @Override
    public void action() {
        try {
            Constants.LOG.info("New phase 2 message");
            String json = reader.readString();
            Constants.LOG.info("Readed header -> " + json);
            ObjectMapper objectMapper = new ObjectMapper();
            MessageToSend readValue = objectMapper.readValue(json, MessageToSend.class);
            File tempFile = File.createTempFile("phase2_", ".tmp");
            reader.readFile(tempFile);
            Constants.LOG.info("Temp file -> " + tempFile.getAbsolutePath());
            ArrayList<WFile> message = readValue.getMessage();
            Constants.LOG.info("User1: " + readValue.getUser1() + " - User2: " + readValue.getUser2());
            /**
             * Every "part" is a part of P (xored).
             */
            boolean recoverablePart = false;
            WFile known = null, unknown = null;
            WFilePhase1 known1 = null;
            Constants.FILES_FROM_PHASE_1_SEM.acquire();
            for (WFile f : message) {
                if (Constants.FILES_FROM_PHASE_1.get(f.getPartID()) != null) {
                    known1 = Constants.FILES_FROM_PHASE_1.get(f.getPartID());
                    known = f;
                } else {
                    unknown = f;
                }
            }
            Constants.FILES_FROM_PHASE_1_SEM.release();
            System.out.println(known + " - " + unknown);
            if (known == null || unknown == null) {
                return;
            }
            Constants.LOG.info(known.getPartID() + " " + unknown.getPartID());
            //Constants.LOG.info(known.getJ().toString() + "," + known.getRelayID() + "," + known.getToRelay() + " - " + unknown.getJ().toString() + "," + unknown.getRelayID() + "," + unknown.getToRelay());
            boolean reallyKnows = false;
            File knownFile = known1.getXoredFile();
            if (knownFile != null) {
                reallyKnows = true;
            }
            Constants.LOG.info("ReallyKnows: " + reallyKnows);
            if (reallyKnows) {
                Constants.LOG.info("Decoding...");
                long skippedBytes = (tempFile.length()) * known.getToRelayCounter();
                System.out.println(known.getTotalRelay() + " - " + known.getToRelayCounter());
                System.out.println(tempFile.length());
                System.out.println(knownFile.length());
                Constants.LOG.info("Skip " + skippedBytes + "B from known. " + known.getOffset() + " " + unknown.getOffset());
                skippedBytes = known.getOffset();
                long bytesToRead = tempFile.length();
                Constants.LOG.info("Read " + bytesToRead + "B");
                File decodedFile = File.createTempFile("decodedPhase2_", ".tmp");
                DataOutputStream out = new DataOutputStream(new FileOutputStream(decodedFile));
                DataInputStream in = new DataInputStream(new FileInputStream(tempFile));
                DataInputStream in2 = new DataInputStream(new FileInputStream(knownFile));
                in2.skip(skippedBytes);
                Constants.LOG.info(skippedBytes + " ----- " + knownFile.length());
                byte[] buffer = new byte[1];
                byte[] buffer2 = new byte[1];
                // unknown is what I want, known is what I have, tempFile is the xored until min
                long maxSize = Math.max(known.getFileLength(), unknown.getFileLength());
                long minSize = Math.min(known.getFileLength(), unknown.getFileLength());
                int readed;
                for (long r = 0; r < minSize; r++) {
                    in.read(buffer);
                    in2.read(buffer2);
                    out.write(new byte[]{xor(buffer[0], buffer2[0])});
                }
                /*
                while ((readed = in.read(buffer)) != -1) {
                    in2.read(buffer2);
                    for (int i = 0; i < readed; i++) {
                        out.write(new byte[]{xor(buffer[i], buffer2[i])});
                    }
                }*/
                if (known.getFileLength() < unknown.getFileLength()) {
                    //if i known less than the known the remaining file is uncoded
                    for (long r = 0; r < maxSize - minSize; r++) {
                        in.read(buffer);
                        out.write(buffer);
                    }
                }
                in2.close();
                in.close();
                out.close();
                Constants.LOG.info("File saved to " + decodedFile.getAbsolutePath());
                Constants.LOG.info("Trying to decode the W file...");
                //TODO decode the w file (unkown)
                decodedFile = decode(decodedFile, unknown, known);
                if (decodedFile == null) {
                    return;
                }
                Constants.LOG.info("File decoded saved to: " + decodedFile.getAbsolutePath());
                Constants.FILES_FROM_PHASE_2_SEM.acquire();
                if (Constants.FILES_FROM_PHASE_2.get(unknown.getPartID()) == null) {
                    WFilePhase2 wFilePhase2 = new WFilePhase2();
                    wFilePhase2.setSubparts(new ArrayList<>());
                    wFilePhase2.setwPartID(unknown.getPartID());
                    wFilePhase2.setTotalParts(unknown.getTotalRelay());
                    Constants.FILES_FROM_PHASE_2.put(unknown.getPartID(), wFilePhase2);
                }
                WFilePhase2Subpart wfp2s = new WFilePhase2Subpart();
                wfp2s.setFile(decodedFile);
                wfp2s.setPartID(unknown.getToRelayCounter());
                Constants.FILES_FROM_PHASE_2.get(unknown.getPartID()).getSubparts().add(wfp2s);
                Constants.FILES_FROM_PHASE_2_SEM.release();
            }

        } catch (IOException | InterruptedException ex) {
            Constants.LOG.error("", ex);
        }
    }

    private File decode(File wfileFile, WFile unknownwfile, WFile knownwfile) throws IOException {

        if (1 == 1) {
            //  return wfileFile;
        }
        File outputFile = File.createTempFile("decodedwfromdecodedPhase2", ".tmp");

        boolean availableToDecode = false;
        ArrayList<Point> files = unknownwfile.getInternal_filesXored();
        int cachedCounter = 0;
        int currentPart = knownwfile.getInternal_currentPart();
        Point decodedFile = null;
        for (Point file : files) {
            int f = file.x;
            int s = file.y;
            if (Constants.CACHED_FILES[f][s]) {
                cachedCounter++;
            } else {
                decodedFile = file;
            }
        }
        availableToDecode = cachedCounter == files.size() - 1;
        Constants.LOG.info("Available: " + availableToDecode + " Decodable: " + decodedFile.x + "," + decodedFile.y);
        //xor the files:
        if (availableToDecode) {
            long bytesToBeSkipped = 0;
            bytesToBeSkipped = unknownwfile.getInternal_offset() + unknownwfile.getOffset();//wfileFile.length() * unknownwfile.getToRelayCounter();
            Constants.LOG.info(unknownwfile.getInternal_offset() + " " + knownwfile.getInternal_fileSize() + " " + currentPart + " " + wfileFile.length() + " " + knownwfile.getToRelayCounter() + " " + knownwfile.getTotalRelay());
            DataInputStream[] ins = new DataInputStream[cachedCounter + 1];
            int counter = 0;
            for (Point file : files) {
                if (file.x == decodedFile.x && file.y == decodedFile.y) {
                    continue;
                }
                ins[counter] = new DataInputStream(new FileInputStream(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y));
                //skip the bytes
                ins[counter++].skip(bytesToBeSkipped);
                if (!new File(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y).exists()) {
                    Constants.LOG.error("File " + Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y + " does not exist");
                }
            }

            Constants.LOG.info("Skip " + bytesToBeSkipped + " bytes");
            ins[counter] = new DataInputStream(new FileInputStream(wfileFile));
            DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(outputFile));
            for (long i = 0; i < wfileFile.length(); i++) {
                Byte result = null;
                for (int j = 0; j < ins.length; j++) {
                    if (j == 0) {
                        result = ins[j].readByte();
                    } else {
                        result = xor(result, ins[j].readByte());
                    }
                }
                outputStream.write(result);
            }
            for (DataInputStream in : ins) {
                in.close();
            }
            outputStream.close();

        } else {
            return null;
        }

        return outputFile;
    }

    private byte xor(byte byte1, byte byte2) {
        return (byte) ((int) byte1 ^ (int) byte2);
    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
