/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions.beans.phase2;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class PToSend {

    private File file;
    int toRelay;
    ArrayList<WFile> subParts = new ArrayList<>();

    public int getToRelay() {
        return toRelay;
    }

    public void setToRelay(int toRelay) {
        this.toRelay = toRelay;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<WFile> getSubParts() {
        return subParts;
    }

    public void setSubParts(ArrayList<WFile> subParts) {
        this.subParts = subParts;
    }

}
