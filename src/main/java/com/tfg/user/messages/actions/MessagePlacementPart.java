/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.user.Constants;
import com.tfg.user.utils.SocketReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePlacementPart implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART;
    private final SocketReader READER;

    public MessagePlacementPart(SocketReader reader) {
        READER = reader;
    }

    @Override
    public void action() {
        try {
            Constants.LOG.info("New part. Reading...");
            String idUsers = READER.readString();
            Constants.LOG.info("Message to " + idUsers);
            int[] users = new ObjectMapper().readValue(idUsers, int[].class);
            boolean itsForMe = false;
            for (int i = 0; i < users.length; i++) {
                if (users[i] == Constants.ID) {
                    itsForMe = true;
                    Constants.LOG.info("The file it's for me");
                    break;
                }
            }

            int fileID = READER.readInt32();
            int subpartID = READER.readInt32();
            Constants.LOG.info("New part " + fileID + "," + subpartID);
            File tempFile = File.createTempFile("subpart", ".tmp");
            READER.readFile(tempFile);
            Constants.LOG.info("Subpart readed ans saved to " + tempFile.getAbsolutePath());
            boolean writeFile = false;
            Constants.SUMMARY_AND_CACHED_FILES_SEM.acquire();
            int[] fileIDs = Constants.SUMMARY.getIds();
            int index = -1;
            for (int i = 0; i < fileIDs.length; i++) {
                if (fileIDs[i] == fileID) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                Constants.LOG.info("File not found");
            } else {
                if (itsForMe) {
                    if (Constants.CACHED_FILES[index][subpartID]) {
                        Constants.LOG.info("Subfile " + fileID + "," + subpartID + " already cached");
                    } else {
                        Constants.CACHED_FILES[index][subpartID] = true;
                        writeFile = true;
                        Constants.LOG.info("Subfile " + fileID + "," + subpartID + " cached");
                    }
                }
            }
            Constants.SUMMARY_AND_CACHED_FILES_SEM.release();
            if (writeFile) {
                new File(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + fileID).mkdirs();
                File file = new File(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + fileID + "/" + subpartID);
                Files.copy(tempFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                tempFile.delete();
                Constants.LOG.info("Subfile saved to " + file.getAbsolutePath());
            }
        } catch (IOException ex) {
            Constants.LOG.error("IOException", ex);
        } catch (InterruptedException ex) {
            Constants.LOG.error("Concurrency error. Exiting...", ex);
        }

    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
