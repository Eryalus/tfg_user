/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions.beans.phase2;

import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class MessageToSend {

    ArrayList<WFile> message;
    int user1, user2;

    public int getUser1() {
        return user1;
    }

    public int getUser2() {
        return user2;
    }

    public void setUser1(int user1) {
        this.user1 = user1;
    }

    public void setUser2(int user2) {
        this.user2 = user2;
    }

    public ArrayList<WFile> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<WFile> message) {
        this.message = message;
    }

}
