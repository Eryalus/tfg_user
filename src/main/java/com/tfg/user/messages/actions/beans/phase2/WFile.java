/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions.beans.phase2;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class WFile {

    private long partID;
    private int totalRelay;
    private int toRelay;
    private int toRelayCounter;
    private long fileLength, offset;

    private int internal_totalParts;
    private int internal_currentPart;
    private long internal_fileSize, internal_offset;
    private ArrayList<Point> internal_filesXored = new ArrayList<>();

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public long getInternal_offset() {
        return internal_offset;
    }

    public void setInternal_offset(long internal_offset) {
        this.internal_offset = internal_offset;
    }

    public long getInternal_fileSize() {
        return internal_fileSize;
    }

    public void setInternal_fileSize(long internal_fileSize) {
        this.internal_fileSize = internal_fileSize;
    }

    public int getInternal_totalParts() {
        return internal_totalParts;
    }

    public void setInternal_totalParts(int internal_totalParts) {
        this.internal_totalParts = internal_totalParts;
    }

    public int getInternal_currentPart() {
        return internal_currentPart;
    }

    public void setInternal_currentPart(int internal_currentPart) {
        this.internal_currentPart = internal_currentPart;
    }

    public ArrayList<Point> getInternal_filesXored() {
        return internal_filesXored;
    }

    public void setInternal_filesXored(ArrayList<Point> internal_filesXored) {
        this.internal_filesXored = internal_filesXored;
    }

    public int getToRelayCounter() {
        return toRelayCounter;
    }

    public void setToRelayCounter(int toRelayCounter) {
        this.toRelayCounter = toRelayCounter;
    }

    public long getPartID() {
        return partID;
    }

    public void setPartID(long partID) {
        this.partID = partID;
    }

    public int getTotalRelay() {
        return totalRelay;
    }

    public void setTotalRelay(int totalRelay) {
        this.totalRelay = totalRelay;
    }

    public int getToRelay() {
        return toRelay;
    }

    public void setToRelay(int toRelay) {
        this.toRelay = toRelay;
    }

}
