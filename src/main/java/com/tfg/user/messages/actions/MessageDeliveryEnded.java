/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions;

import com.tfg.user.Constants;
import com.tfg.user.files.FileReconstructor;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author eryalus
 */
public class MessageDeliveryEnded implements MessageAction {

    @Override
    public void action() {
        try {
            boolean release = false;
            Constants.IS_DELIVERY_ENDED_SEM.acquire();
            Constants.DELIVERY_ENDED_COUNTER = Constants.DELIVERY_ENDED_COUNTER + 1;
            if (Constants.DELIVERY_ENDED_COUNTER == Constants.RELAYS_IPS.size()) {
                Constants.IS_DELIVERY_ENDED = true;
            }
            if (Constants.IS_DELIVERY_ENDED) {
                Constants.DELIVERY_ENDED_COUNTER = 0;
                release = true;
                Constants.IS_DELIVERY_ENDED = true;
                Constants.LOG.info("DELIVERY ENDED!!!");
                Constants.LOG.info("Reconstruct the file...");
                new FileReconstructor().reconstruct(Constants.SELECTED_FILE);
            }
            Constants.IS_DELIVERY_ENDED_SEM.release();
            if (release) {
                Constants.DELIVERY_ENDED_SEM.release();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MessageDeliveryEnded.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int getACTION() {
        return Constants.MESSAGE_ACTIONS_TYPE.DELIVEY_ENDED;
    }

}
