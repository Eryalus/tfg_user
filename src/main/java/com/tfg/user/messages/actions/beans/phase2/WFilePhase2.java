/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions.beans.phase2;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class WFilePhase2 {

    private long wPartID;
    private int totalParts;
    private ArrayList<WFilePhase2Subpart> subparts = new ArrayList<>();

    public long getwPartID() {
        return wPartID;
    }

    public void setwPartID(long wPartID) {
        this.wPartID = wPartID;
    }

    public int getTotalParts() {
        return totalParts;
    }

    public void setTotalParts(int totalParts) {
        this.totalParts = totalParts;
    }

    public ArrayList<WFilePhase2Subpart> getSubparts() {
        return subparts;
    }

    public void setSubparts(ArrayList<WFilePhase2Subpart> subparts) {
        this.subparts = subparts;
    }

}
