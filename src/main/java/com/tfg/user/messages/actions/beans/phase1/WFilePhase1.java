/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions.beans.phase1;

import java.awt.Point;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class WFilePhase1 {

    private File file, xoredFile;
    private int fileID, subfileID, part, totalParts;
    private long partID;
    private Point[] xoredFiles;

    public long getPartID() {
        return partID;
    }

    public void setPartID(long partID) {
        this.partID = partID;
    }

    private int[] J;
    private int subindexRelay;

    public File getXoredFile() {
        return xoredFile;
    }

    public void setXoredFile(File xoredFile) {
        this.xoredFile = xoredFile;
    }

    public Point[] getXoredFiles() {
        return xoredFiles;
    }

    public void setXoredFiles(Point[] xoredFiles) {
        this.xoredFiles = xoredFiles;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getFileID() {
        return fileID;
    }

    public void setFileID(int fileID) {
        this.fileID = fileID;
    }

    public int getSubfileID() {
        return subfileID;
    }

    public void setSubfileID(int subfileID) {
        this.subfileID = subfileID;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public int getTotalParts() {
        return totalParts;
    }

    public void setTotalParts(int totalParts) {
        this.totalParts = totalParts;
    }

}
