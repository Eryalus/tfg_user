/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.user.Constants;
import com.tfg.user.files.ReadedW;
import com.tfg.user.messages.actions.beans.phase1.WFilePhase1;
import com.tfg.user.utils.SocketReader;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePhase1Part implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE;
    private final SocketReader reader;

    public MessagePhase1Part(SocketReader reader) {
        this.reader = reader;
    }

    @Override
    public void action() {
        try {
            Constants.LOG.info("New phase 1 message");
            int relayID = reader.readInt32();
            long partID = reader.readLong();
            System.out.println("ID: " + partID);
            int totalParts = reader.readInt32();
            int currentPart = reader.readInt32();
            long fileOffset = reader.readLong();
            String xoredFiles = reader.readString();
            String J = reader.readString();
            File temp = File.createTempFile("readerPhase1", ".tmp");
            reader.readFile(temp);
            Constants.LOG.info("Readed info -> totalParts:" + totalParts + " currentParts:" + currentPart + " xorFiles:" + xoredFiles + " saved to " + temp.getAbsolutePath());
            //decode the json
            Point[] files = new ObjectMapper().readValue(xoredFiles, Point[].class);
            Integer[] J_V = new ObjectMapper().readValue(J, Integer[].class);
            ReadedW readedW = new ReadedW();
            readedW.setFile(temp);
            ArrayList<Integer> J_V_AL = new ArrayList<>();
            for (Integer i : J_V) {
                J_V_AL.add(i);
            }
            readedW.setJ(J_V_AL);
            readedW.setTotalParts(totalParts);
            readedW.setTotalParts(relayID);
            Constants.LOG.info("Readed: " + J_V_AL.toString() + " " + totalParts + " " + relayID);
            try {
                Constants.READED_W_SEM.acquire();
                Constants.READED_W.add(readedW);
                Constants.READED_W_SEM.release();
            } catch (InterruptedException ex) {
                Logger.getLogger(MessagePhase1Part.class.getName()).log(Level.SEVERE, null, ex);
            }

            boolean availableToDecode;
            int cachedCounter = 0;
            Point decodedFile = null;
            for (Point file : files) {
                int f = file.x;
                int s = file.y;
                if (Constants.CACHED_FILES[f][s]) {
                    cachedCounter++;
                } else {
                    decodedFile = file;
                }
            }
            availableToDecode = cachedCounter == files.length - 1;
            Constants.LOG.info("Available: " + availableToDecode + " Decodable: " + decodedFile.x + "," + decodedFile.y);
            //xor the files:
            if (availableToDecode) {
                long bytesToBeSkipped = 0;
                bytesToBeSkipped = (temp.length()) * currentPart;
                bytesToBeSkipped = fileOffset;
                DataInputStream[] ins = new DataInputStream[cachedCounter + 1];
                int counter = 0;
                for (Point file : files) {
                    if (file.x == decodedFile.x && file.y == decodedFile.y) {
                        continue;
                    }
                    ins[counter] = new DataInputStream(new FileInputStream(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y));
                    //skip the bytes
                    ins[counter++].skip(bytesToBeSkipped);
                    if (!new File(Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y).exists()) {
                        Constants.LOG.error("File " + Constants.SUBFILES_PATH + "/user" + Constants.ID + "/" + file.x + "/" + file.y + " does not exist");
                    }
                }
                Constants.LOG.info("Skip " + bytesToBeSkipped + " bytes of " + (Constants.SUMMARY.getFileSize() / Constants.SUMMARY.getNumberOfFiles()) + " Offset: " + fileOffset);
                
                ins[counter] = new DataInputStream(new FileInputStream(temp));
                File outputFile = File.createTempFile("wdecoded", ".tmp");
                DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(outputFile));
                for (long i = 0; i < temp.length(); i++) {
                    Byte result = null;
                    for (int j = 0; j < ins.length; j++) {
                        if (j == 0) {
                            result = ins[j].readByte();
                        } else {
                            result = xor(result, ins[j].readByte());
                        }
                    }
                    outputStream.write(result);
                }
                for (DataInputStream in : ins) {
                    in.close();
                }
                outputStream.close();
                WFilePhase1 wfp1 = new WFilePhase1();
                wfp1.setFile(outputFile);
                wfp1.setFileID(decodedFile.x);
                wfp1.setSubfileID(decodedFile.y);
                wfp1.setTotalParts(totalParts);
                wfp1.setPart(currentPart);
                wfp1.setXoredFile(temp);
                wfp1.setXoredFiles(files);
                wfp1.setPartID(partID);
                try {
                    Constants.W_FILES_SEM.acquire();
                    Constants.W_FILES.add(wfp1);
                    Constants.W_FILES_SEM.release();
                    Constants.FILES_FROM_PHASE_1_SEM.acquire();
                    Constants.FILES_FROM_PHASE_1.put(partID, wfp1);
                    Constants.FILES_FROM_PHASE_1_SEM.release();
                } catch (InterruptedException ex) {
                    Constants.LOG.error("", ex);
                }

                Constants.LOG.info("File " + wfp1.getFileID() + "," + wfp1.getSubfileID() + "," + wfp1.getPart() + "/" + wfp1.getTotalParts() + " saved to: " + outputFile.getAbsolutePath());

            } else {
                WFilePhase1 wfp1 = new WFilePhase1();
                wfp1.setFile(null);
                wfp1.setFileID(decodedFile.x);
                wfp1.setSubfileID(decodedFile.y);
                wfp1.setTotalParts(totalParts);
                wfp1.setPart(currentPart);
                wfp1.setXoredFile(temp);
                wfp1.setXoredFiles(files);
                wfp1.setPartID(partID);
                try {
                    Constants.FILES_FROM_PHASE_1_SEM.acquire();
                    Constants.FILES_FROM_PHASE_1.put(partID, wfp1);
                    Constants.FILES_FROM_PHASE_1_SEM.release();
                } catch (InterruptedException ex) {
                    Logger.getLogger(MessagePhase1Part.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    private byte xor(byte byte1, byte byte2) {
        return (byte) ((int) byte1 ^ (int) byte2);
    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
