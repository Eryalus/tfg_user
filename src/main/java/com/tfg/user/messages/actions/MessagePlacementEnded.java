/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.messages.actions;

import com.tfg.user.Constants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePlacementEnded implements MessageAction {

    @Override
    public void action() {
        try {
            boolean release = false;
            Constants.IS_POPULATING_ENDED_SEM.acquire();
            if (!Constants.IS_POPULATING_ENDED) {
                release = true;
                Constants.IS_POPULATING_ENDED = true;
            }
            Constants.IS_POPULATING_ENDED_SEM.release();
            if (release) {
                Constants.POPULATING_ENDED_SEM.release();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MessagePlacementEnded.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int getACTION() {
        return Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED;
    }

}
