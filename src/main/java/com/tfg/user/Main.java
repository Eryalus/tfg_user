/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.user.files.FilesSummary;
import com.tfg.user.utils.SocketRW;
import com.tfg.user.utils.SocketReader;
import com.tfg.user.utils.SocketWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eryalus
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        if (args.length != 2) {
            Constants.LOG.error("java -jar user.jar [IP] [id]");
            return;
        }
        try {
            Constants.ID = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            Constants.LOG.error("java -jar user.jar [IP] [id]");
            return;
        }
        Constants.LOG.info("Opening connection with the server...");
        Socket socket;
        SocketReader reader;
        SocketWriter writer;
        try {
            socket = new Socket(args[0], Constants.SERVER_PORT);
            reader = new SocketReader(socket.getInputStream());
            writer = new SocketWriter(socket.getOutputStream());
        } catch (IOException ex) {
            Constants.LOG.error("Can not open connection", ex);
            return;
        }
        Constants.LOG.info("Connection opened.");
        SocketRW rw = new SocketRW(reader, writer);
        rw.setSocket(socket);
        Constants.SOCKET_RW = rw;
        try {
            setup();
        } catch (IOException ex) {
            Constants.LOG.error("IOException", ex);
            return;
        }
        Constants.LOG.info("Waiting for populating to end...");
        Constants.POPULATING_ENDED_SEM.acquire();
        Constants.LOG.info("Populating ended.");
        //menu
        Scanner scan = new Scanner(System.in);
        while (true) {
            int counter = 0;
            System.out.println("-1. Exit");
            while (counter < Constants.SUMMARY.getFiles().size()) {
                System.out.println(Constants.SUMMARY.getIds()[counter++] + ". " + Constants.SUMMARY.getFiles().get(counter - 1));
            }
            System.out.print("Option>");
            int op = scan.nextInt();
            if (op == -1) {
                System.exit(0);
            } else {
                int index = -1;
                for (int i = 0; i < Constants.SUMMARY.getIds().length; i++) {
                    if (Constants.SUMMARY.getIds()[i] == op) {
                        index = i;
                        break;
                    }
                }
                if (index == -1) {
                    continue;
                }
                try {
                    Constants.IS_DELIVERY_ENDED_SEM.acquire();
                    Constants.IS_DELIVERY_ENDED = false;
                    Constants.IS_DELIVERY_ENDED_SEM.release();
                    Constants.SOCKET_RW.getWriter().writeInt32(op);
                    Constants.SELECTED_FILE = op;
                    Constants.SOCKET_RW.getWriter().flush();
                    Constants.LOG.info("Petición enviada.");

                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static void setup() throws IOException {
        SocketWriter writer = Constants.SOCKET_RW.getWriter();
        SocketReader reader = Constants.SOCKET_RW.getReader();
        writer.writeInt32(Constants.ID);
        writer.flush();
        String multicastConnections = reader.readString();
        Constants.LOG.info("Readed multicast connections: " + multicastConnections);
        String[] ip_ports = multicastConnections.split(",");
        ArrayList<MulticastListener> listeners = new ArrayList<>();
        for (String ip_port1 : ip_ports) {
            String[] ip_port = ip_port1.split(":");
            Constants.LOG.info("Opening connection with " + ip_port1 + "...");
            Constants.RELAYS_IPS.add(ip_port[0]);
            MulticastSocket multicastSocket = new MulticastSocket(Integer.parseInt(ip_port[1]));
            Constants.RELAYS_PORTS.add(Integer.parseInt(ip_port[1]));
            InetAddress group = InetAddress.getByName(ip_port[0]);
            multicastSocket.joinGroup(group);
            SocketReader socketReader = new SocketReader(multicastSocket);
            Constants.LOG.info("Connection opened.");
            listeners.add(new MulticastListener(socketReader, ip_port[0]));
        }

        writer.writeLong(1);
        writer.flush();
        Constants.LOG.info("ACK sended.");
        String summary = reader.readString();
        Constants.LOG.info("Summary readed: " + summary);
        Constants.SUMMARY = new ObjectMapper().readValue(summary, FilesSummary.class);
        Constants.CACHED_FILES = new boolean[Constants.SUMMARY.getFiles().size()][Constants.SUMMARY.getNumberOfFiles()];
        for (MulticastListener listener : listeners) {
            new Thread(listener).start();
        }
        Constants.LOG.info("Listeners started");
        writer.writeLong(1);
        writer.flush();
    }
}
