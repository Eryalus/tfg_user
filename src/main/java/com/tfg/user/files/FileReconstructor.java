/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.files;

import com.tfg.user.Constants;
import com.tfg.user.messages.actions.beans.phase1.WFilePhase1;
import com.tfg.user.messages.actions.beans.phase2.WFilePhase2;
import com.tfg.user.messages.actions.beans.phase2.WFilePhase2Subpart;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class FileReconstructor {

    long bytes = 0;

    public void reconstruct(int fileID) {
        Constants.LOG.info("Selected file " + fileID + " " + Constants.SUMMARY.getFiles().get(fileID));
        File file = new File("Downloaded_" + Constants.SUMMARY.getFiles().get(fileID));
        DataOutputStream out;
        try {
            out = new DataOutputStream(new FileOutputStream(file));
        } catch (FileNotFoundException ex) {
            Constants.LOG.error("", ex);
            return;
        }
        ArrayList<Long> FILES_FROM_PHASE_2_KEYS = new ArrayList<>();
        int FILES_FROM_PHASE_2_KEY_INDEX = 0;
        for (Long l : Constants.FILES_FROM_PHASE_2.keySet()) {
            FILES_FROM_PHASE_2_KEYS.add(l);
        }
        Collections.sort(FILES_FROM_PHASE_2_KEYS);
        // Collections.reverse(FILES_FROM_PHASE_2_KEYS);
        Constants.LOG.info(FILES_FROM_PHASE_2_KEYS.size() + " - " + FILES_FROM_PHASE_2_KEYS.toString());
        boolean[] subparts = Constants.CACHED_FILES[fileID];
        boolean exit = false;
        for (int i = 0; i < subparts.length; i++) {
            Constants.LOG.info("Subfile " + i + ". Found? -> " + subparts[i]);
            if (!subparts[i]) {
                Collection<WFilePhase1> Phase1Messages = Constants.FILES_FROM_PHASE_1.values();
                ArrayList<WFilePhase1> ofFile = new ArrayList<>();
                Integer totalParts = null;
                for (WFilePhase1 p1m : Phase1Messages) {
                    if (p1m.getFileID() == fileID && p1m.getSubfileID() == i) {
                        ofFile.add(p1m);
                        totalParts = p1m.getTotalParts();
                    }
                }
                Constants.LOG.info("\tFound " + ofFile.size() + " parts out of " + totalParts);
                if (totalParts != null) {
                    for (int counter = 0; counter < totalParts; counter++) {
                        WFilePhase1 wfp1 = null;
                        for (WFilePhase1 temp : ofFile) {
                            if (temp.getPart() == counter) {
                                wfp1 = temp;
                                break;
                            }
                        }
                        if (wfp1 == null) {
                            Constants.LOG.info("\t\tPart " + counter + " not found...");
                            WFilePhase2 wfp2 = Constants.FILES_FROM_PHASE_2.get(FILES_FROM_PHASE_2_KEYS.get(FILES_FROM_PHASE_2_KEY_INDEX++));
                            int wfp2_totalParts = wfp2.getTotalParts();
                            Constants.LOG.info("\t\t\tDivided into " + wfp2_totalParts + " subparts (" + wfp2.getwPartID() + ")");
                            for (int wfp2s_index = 0; wfp2s_index < wfp2_totalParts; wfp2s_index++) {
                                WFilePhase2Subpart wfp2s = null;
                                for (WFilePhase2Subpart t : wfp2.getSubparts()) {
                                    if (t.getPartID() == wfp2s_index) {
                                        wfp2s = t;
                                        break;
                                    }
                                }
                                if (wfp2s == null) {
                                    Constants.LOG.info("\t\t\t\tSubpart " + wfp2s_index + " not found");
                                } else {
                                    Constants.LOG.info("\t\t\t\tSubpart " + wfp2s_index + " found. Writing... " + wfp2s.getFile().length());
                                    try {
                                        copyToOutput(wfp2s.getFile(), out, 4);
                                    } catch (IOException ex) {
                                        Constants.LOG.error("", ex);
                                        return;
                                    }
                                }
                            }

                            //exit = true;
                            //break;
                        } else {
                            Constants.LOG.info("\t\tSaving part " + counter + "..." + wfp1.getFileID() + "-" + wfp1.getSubfileID() + ";" + wfp1.getPart() + ";" + wfp1.getPartID());
                            try {
                                copyToOutput(wfp1.getFile(), out, 2);
                            } catch (IOException ex) {
                                Constants.LOG.error("", ex);
                                return;
                            }
                        }
                    }
                } else {
                    //TODO when not subfile parts are found
                    //How many parts does it have? -> take it from subpart
                    WFilePhase2 wfp2 = Constants.FILES_FROM_PHASE_2.get(FILES_FROM_PHASE_2_KEYS.get(FILES_FROM_PHASE_2_KEY_INDEX++));
                    //wfp2.get
                    int wfp2_totalParts = wfp2.getTotalParts();
                    Constants.LOG.info("\t\t\tDivided into " + wfp2_totalParts + " subparts (" + wfp2.getwPartID() + ")");
                    for (int wfp2s_index = 0; wfp2s_index < wfp2_totalParts; wfp2s_index++) {
                        WFilePhase2Subpart wfp2s = null;
                        for (WFilePhase2Subpart t : wfp2.getSubparts()) {
                            if (t.getPartID() == wfp2s_index) {
                                wfp2s = t;
                                break;
                            }
                        }
                        if (wfp2s == null) {
                            Constants.LOG.info("\t\t\t\tSubpart " + wfp2s_index + " not found");
                        } else {
                            Constants.LOG.info("\t\t\t\tSubpart " + wfp2s_index + " found. Writing... " + wfp2s.getFile().length());
                            try {
                                copyToOutput(wfp2s.getFile(), out, 4);
                            } catch (IOException ex) {
                                Constants.LOG.error("", ex);
                                return;
                            }
                        }
                    }
                }
            } else {
                try {
                    copyToOutput(new File("subfiles/user" + Constants.ID + "/" + fileID + "/" + i), out, 0);
                } catch (IOException ex) {
                    Constants.LOG.error("", ex);
                    return;
                }
            }
            if (exit) {
                break;
            }
        }
        Constants.LOG.info("USED subfiles: " + FILES_FROM_PHASE_2_KEY_INDEX);
        try {
            out.close();
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    private void copyToOutput(File file, DataOutputStream out, int padding) throws FileNotFoundException, IOException {
        String p = "";
        for (int i = 0; i < padding; i++) {
            p += "\t";
        }
        Constants.LOG.info(p + "Start writing at " + bytes);
        byte[] buff = new byte[2048];
        int readed;
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        while ((readed = in.read(buff)) != -1) {
            bytes += readed;
            out.write(buff, 0, readed);
        }
        in.close();
        Constants.LOG.info(p + "Stopping at " + bytes);
    }
}
