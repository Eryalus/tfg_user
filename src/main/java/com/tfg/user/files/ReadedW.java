/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.files;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class ReadedW {

    int totalParts, toRelay;
    ArrayList<Integer> J;
    File file;

    public int getTotalParts() {
        return totalParts;
    }

    public void setTotalParts(int totalParts) {
        this.totalParts = totalParts;
    }

    public int getToRelay() {
        return toRelay;
    }

    public void setToRelay(int toRelay) {
        this.toRelay = toRelay;
    }

    public ArrayList<Integer> getJ() {
        return J;
    }

    public void setJ(ArrayList<Integer> J) {
        this.J = J;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

}
