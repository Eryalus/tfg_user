/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.files;

import java.util.ArrayList;

/**
 *
 * @author Eryalus
 */
public class FilesSummary {

    private ArrayList<String> files;
    private int[] ids;
    private long fileSize;
    private int numberOfFiles;

    public FilesSummary() {

    }

    public FilesSummary(ArrayList<String> files, int[] ids, long fileSize, int numberOfFiles) {
        this.files = files;
        this.ids = ids;
        this.fileSize = fileSize;
        this.numberOfFiles = numberOfFiles;
    }

    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }

    public ArrayList<String> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<String> files) {
        this.files = files;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getNumberOfFiles() {
        return numberOfFiles;
    }

    public void setNumberOfFiles(int numberOfFiles) {
        this.numberOfFiles = numberOfFiles;
    }
}
