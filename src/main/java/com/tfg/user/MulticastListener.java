/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user;

import com.tfg.user.messages.MessageProcessor;
import com.tfg.user.utils.SocketReader;
import java.io.IOException;

/**
 *
 * @author eryalus
 */
public class MulticastListener implements Runnable {

    private final SocketReader READER;
    private final String IP;
    MessageProcessor messageProcessor;

    public MulticastListener(SocketReader reader, String listeningIP) {
        READER = reader;
        IP = listeningIP;
        messageProcessor = new MessageProcessor(reader);
    }

    @Override
    public void run() {
        Constants.LOG.info("Listener for " + IP + " started");
        while (true) {
            try {
                //all of the messages will start with an int32 as message type
                int type = READER.readInt32();
                Constants.LOG.info("Incoming message. Type:" + type);
                messageProcessor.readMessageForType(type);
            } catch (IOException ex) {
                Constants.LOG.error("IOExcepcion", ex);
            }
        }
    }

}
