/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.utils;

import com.tfg.user.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MulticastInputStream extends InputStream implements Runnable {

    private static final int DATAGRAM_PACKET_SIZE = 65000;
    private final LinkedList<Byte> queue;
    private final Semaphore sem;
    private final MulticastSocket socket;
    private final DatagramSocket datagramSocket;
    private final InetAddress[] group;
    private long bytesReaded = 0;
    private HashMap<Long, ReadedPackage> UnattendedPackages = new HashMap<>();
    private long lastPackageAdded = -1L;

    public long getBytesReaded() {
        return bytesReaded;
    }

    public MulticastInputStream(MulticastSocket socket) throws UnknownHostException, SocketException {
        super();
        this.queue = new LinkedList<>();
        sem = new Semaphore(1);
        this.socket = socket;
        this.group = new InetAddress[Constants.RELAYS_IPS.size()];
        for (int i = 0; i < this.group.length; i++) {
            this.group[i] = InetAddress.getByName(Constants.RELAYS_IPS.get(i));
        }
        datagramSocket = new DatagramSocket();
    }

    private void push(byte b) {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        bytesReaded++;
        queue.addFirst(b);
        sem.release();
    }

    private void push(byte[] bs) {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Byte b : bs) {
            bytesReaded++;
            queue.addFirst(b);
        }
        sem.release();
    }

    private byte pop() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        Byte removeLast = queue.removeLast();
        sem.release();
        return removeLast;
    }

    @Override
    public int read() throws IOException {
        waitFor(1);
        return pop();
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        waitFor(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = pop();
        }
        return bytes.length;
    }

    @Override
    public int available() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        int size = queue.size();
        sem.release();
        return size;
    }

    private void waitFor(int bytes_size) throws IOException {
        while (available() < bytes_size) {
            try {
                synchronized (this) {
                    wait(100);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(SocketReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        byte[] secNumber = new byte[8];
        long number;
        byte[] buf = new byte[DATAGRAM_PACKET_SIZE];
        DatagramPacket p = new DatagramPacket(buf, DATAGRAM_PACKET_SIZE);
        while (true) {
            try {
                socket.receive(p);
                ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
                System.arraycopy(p.getData(), 0, secNumber, 0, 8);
                buffer.put(secNumber);
                buffer.flip();
                number = buffer.getLong();
                if (p.getLength() == 24 && number == -1) {
                    continue;
                }
                byte[] temp = new byte[p.getLength() - 8];
                for (int i = 8; i < p.getLength(); i++) {
                    temp[i - 8] = p.getData()[i];
                }
                if (number == lastPackageAdded + 1) {
                    push(temp);
                    lastPackageAdded = number;
                } else {
                    UnattendedPackages.put(number, new ReadedPackage(number, temp));
                }
                boolean checkNext;
                do {
                    checkNext = false;
                    ArrayList<Long> toRemove = new ArrayList<>();
                    for (Long key : UnattendedPackages.keySet()) {
                        if (key == lastPackageAdded + 1) {
                            push(UnattendedPackages.get(key).ReadedBytes);
                            lastPackageAdded = key;
                            checkNext = true;
                            toRemove.add(key);
                            break;
                        } else if (key < lastPackageAdded) {
                            toRemove.add(key);
                        }
                    }
                    for (Long key : toRemove) {
                        UnattendedPackages.remove(key);
                    }
                } while (checkNext);
                //send ack
                ByteBuffer mBlockBuffer = ByteBuffer.allocate(Long.BYTES);
                mBlockBuffer.putLong(-1L);
                ByteBuffer idBuffer = ByteBuffer.allocate(Long.BYTES);
                idBuffer.putLong(Constants.ID);
                ByteBuffer secBuffer = ByteBuffer.allocate(Long.BYTES);
                secBuffer.putLong(number);
                byte[] bf = new byte[Long.BYTES * 3];
                for (int i = 0; i < 8; i++) {
                    bf[i] = mBlockBuffer.array()[i];
                }
                for (int i = 8; i < 16; i++) {
                    bf[i] = idBuffer.array()[i - 8];
                }
                for (int i = 16; i < 24; i++) {
                    bf[i] = secBuffer.array()[i - 16];
                }
                for (int i = 0; i < this.group.length; i++) {
                    DatagramPacket pack = new DatagramPacket(bf, bf.length, this.group[i], Constants.RELAYS_PORTS.get(i));
                    datagramSocket.send(pack);
                }
            } catch (IOException ex) {
                Logger.getLogger(MulticastInputStream.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class ReadedPackage {

        private final byte[] ReadedBytes;
        private final long SecuenceNumber;

        public ReadedPackage(long secuenceNumber, byte[] readedBytes) {
            SecuenceNumber = secuenceNumber;
            ReadedBytes = readedBytes;
        }

        public byte[] getReadedBytes() {
            return ReadedBytes;
        }

        public long getSecuenceNumber() {
            return SecuenceNumber;
        }

    }
}
