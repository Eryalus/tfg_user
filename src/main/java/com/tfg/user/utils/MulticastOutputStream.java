package com.tfg.user.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.tfg.user.Constants;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MulticastOutputStream extends OutputStream {

    private static final int DATAGRAM_PACKET_SIZE = 1024;
    private final LinkedList<Byte> queue;
    private final Semaphore sem;
    private final DatagramSocket socket;
    private final InetAddress group;
    private long bytesAdded = 0, bytesSended = 0;
    private long secuenceNumber = 0;

    public long getBytesAdded() {
        return bytesAdded;
    }

    public long getBytesSended() {
        return bytesSended;
    }

    public MulticastOutputStream(DatagramSocket socket, String ip) throws UnknownHostException {
        super();
        this.group = InetAddress.getByName(ip);
        this.queue = new LinkedList<>();
        sem = new Semaphore(1);
        this.socket = socket;
    }

    private void push(byte b) throws IOException {
        bytesAdded++;
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        queue.addFirst(b);
        sem.release();
        if (queue.size() >= DATAGRAM_PACKET_SIZE) {
            sendData();
        }
    }

    private byte pop() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        Byte removeLast = queue.removeLast();
        sem.release();
        return removeLast;
    }

    @Override
    public void flush() throws IOException {
        sendData();
    }

    private void sendData() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(secuenceNumber++);
        buffer.array();
        byte[] buf = new byte[queue.size()+buffer.array().length];
        for(int i=0;i<buffer.array().length;i++){
            buf[i]=buffer.array()[i];
        }
        for (int i = buffer.array().length; i < buf.length; i++) {
            buf[i] = pop();
        }
        bytesSended += buf.length;
        DatagramPacket p = new DatagramPacket(buf, buf.length, group, Constants.SERVER_MULTICAST_PORT);

        socket.send(p);
    }

    @Override
    public void write(int b) throws IOException {
        push((byte) b);
    }

    @Override
    public void write(byte[] bs) throws IOException {
        for (Byte b : bs) {
            push(b);
        }
    }

}
