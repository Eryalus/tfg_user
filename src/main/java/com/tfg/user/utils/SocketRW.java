/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user.utils;

import java.net.Socket;

/**
 *
 * @author Eryalus
 */
public class SocketRW {

    private final SocketReader reader;
    private final SocketWriter writer;
    private Socket socket = null;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public SocketReader getReader() {
        return reader;
    }

    public SocketWriter getWriter() {
        return writer;
    }

    public SocketRW(SocketReader reader, SocketWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }
}
