/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.user;

import com.tfg.user.files.FilesSummary;
import com.tfg.user.files.ReadedW;
import com.tfg.user.messages.actions.beans.phase1.WFilePhase1;
import com.tfg.user.messages.actions.beans.phase2.WFilePhase2;
import com.tfg.user.utils.SocketRW;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eryalus
 */
public class Constants {

    public static int ID;
    public static final String TOPOLOGY_JSON_PATH = "res/topology.json";
    public static final String FILES_JSON_PATH = "res/files.json";
    public static final String SUBFILES_PATH = "subfiles";
    public static final String ORIGINAL_FILES_PATH = "downloaded_files";
    public static final Logger LOG = LoggerFactory.getLogger("TFG User");
    public static int SERVER_PORT = 2000;
    public static int SERVER_MULTICAST_PORT = 4446;
    public static SocketRW SOCKET_RW;
    public static FilesSummary SUMMARY;
    public static Semaphore SUMMARY_AND_CACHED_FILES_SEM = new Semaphore(1);
    public static boolean[][] CACHED_FILES;
    public static ArrayList<String> RELAYS_IPS = new ArrayList<>();
    public static ArrayList<Integer> RELAYS_PORTS = new ArrayList<>();
    public static final ArrayList<WFilePhase1> W_FILES = new ArrayList<>();
    public static final ArrayList<ReadedW> READED_W = new ArrayList<>();
    public static final HashMap<Long, WFilePhase1> FILES_FROM_PHASE_1 = new HashMap<>();
    public static final HashMap<Long, WFilePhase2> FILES_FROM_PHASE_2 = new HashMap<>();
    public static int SELECTED_FILE;
    public static int DELIVERY_ENDED_COUNTER = 0;

    public static boolean IS_POPULATING_ENDED = false;
    public static boolean IS_DELIVERY_ENDED = false;
    public static final Semaphore POPULATING_ENDED_SEM = new Semaphore(0);
    public static final Semaphore IS_POPULATING_ENDED_SEM = new Semaphore(1);
    public static final Semaphore DELIVERY_ENDED_SEM = new Semaphore(0);
    public static final Semaphore IS_DELIVERY_ENDED_SEM = new Semaphore(1);
    public static final Semaphore W_FILES_SEM = new Semaphore(1);
    public static final Semaphore READED_W_SEM = new Semaphore(1);
    public static final Semaphore FILES_FROM_PHASE_1_SEM = new Semaphore(1);
    public static final Semaphore FILES_FROM_PHASE_2_SEM = new Semaphore(1);

    public static final class MESSAGE_ACTIONS_TYPE {

        public static final int SETUP = 1, PLACEMENT_PART = 2, PLACEMENT_ENDED = 3, PHASE_1_MESSAGE = 4, PHASE_2_MESSAGE = 5, DELIVEY_ENDED = 6;

    }
}
